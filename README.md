# Generative AI Classwork Repository

Welcome to the Generative AI classwork repository! This repository contains coursework and code developed during the Women Engineers program Module 3 classes, focusing on Generative AI

## Classwork Highlights

During the course, we engaged in various hands-on activities and discussions centered around Generative AI. Here are some key highlights of the classwork:

1. **Computational Thinking Challenges**: We tackled computational thinking questions using Generative AI techniques,tried implementing the clean code.

2. **Yatzee Game Scorer**: As part of our practical sessions, we implemented and played the Yatzee game with a Generative AI (GenAI). This interactive exercise provided insights into decision-making processes and AI interaction.

3. **Yahtzee Code Development and Testing**: We wrote code to implement the Yahtzee game logic and devised comprehensive testing strategies to validate our implementations. Through iterative development and testing cycles, we refined our coding skills and software testing techniques.

4. **Markov Chain Text Generation**: A significant portion of our coursework involved implementing a Markov chain model for text generation. By training the model on textual data, we explored the fascinating realm of probabilistic models and their applications in natural language processing.



## Contributions and Feedback


We encourage contributions and feedback from fellow participants and enthusiasts interested in Generative AI and related topics. Whether you have suggestions for improvement, want to share your insights, or wish to contribute code enhancements, your input is valuable to us.

Feel free to submit pull requests, raise issues, or engage in discussions to collaborate and enhance our collective learning experience.
